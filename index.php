<?php
$outputFile = 'results.csv';
$almostJson = file_get_contents('data.almost_json');

$json = str_replace(array("[","]"),array("",","), $almostJson);
$json = preg_replace('/:(\w*)=>/i','"${1}":', $json);

$json = str_replace("=>",":",substr(trim($json),0,-1));

$json= "[".$json."]";

$objArr = json_decode($json);

if(file_exists($outputFile)) {
    unlink($outputFile);
}

$file_input = fopen($outputFile,"w");

foreach ($objArr as $index => $obj) {
    foreach ($obj as $name => $subObj) {
        
        $columns = get_object_vars($subObj);
        $arr = array();

        $arr['parent'] = $name;
        
        foreach ($columns as $columnName => $columnValue) {
            $arr[$columnName] = $columnValue;
        }

        if ($index == 0) {
            fputcsv($file_input, array_keys($arr));
        }

        fputcsv($file_input, $arr);

    }
}

fclose($file_input);



