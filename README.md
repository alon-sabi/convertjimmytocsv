# Repository to convert rails model "json" into CSV

This is a simple script to convert rails data dump into a CSV file.

## Usage:

```
cp data.almost_json.template data.almost_json
```

Paste the output from rails into the ``` data.almost_json ``` file:

[{"name"=>{:column1=>x, :coumn2=>y}, {"name2"=>{:column1=>a, :column2=>b}}]

NOTE: If you have multiple results, just paste them one after the other like so (do not add commas or anything):

[{"name"=>{:column1=>x, :coumn2=>y}, {"name2"=>{:column1=>a, :column2=>b}}]
[{"name"=>{:column1=>x, :coumn2=>y}, {"name2"=>{:column1=>a, :column2=>b}}]
[{"name"=>{:column1=>x, :coumn2=>y}, {"name2"=>{:column1=>a, :column2=>b}}]

Open up the terminal and type:

```
php index.php
```

You should have a new .csv file named ``` results.csv ```
